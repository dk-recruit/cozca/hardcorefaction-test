package fr.arthursrv.hcf.faction;

import java.sql.ResultSet;
import java.util.UUID;

import org.bukkit.entity.Player;

public class FactionPlayer {
	private int balance;
	private final UUID id;
	private Faction faction;
	
	public FactionPlayer(Player player, int balance, Faction faction) {
		this.id = player.getUniqueId();
		this.balance = balance;
		this.faction = faction;
		
	}
	
	public FactionPlayer(ResultSet set, FactionManager manager) throws Exception {
		this.id = UUID.fromString(set.getString("uuid"));
		this.balance = set.getInt("balance");
		this.faction = manager.getFaction(set.getString("faction"));
	}

	public int getBalance() {
		return balance;
	}
	
	public Faction getFaction() {
		return faction;
	}
	
	public UUID getId() {
		return id;
	}

	public String getFactionName() {
		return faction == null ? "null" : faction.getName();
	}
}

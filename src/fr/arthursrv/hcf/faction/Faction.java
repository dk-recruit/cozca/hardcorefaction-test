package fr.arthursrv.hcf.faction;

import java.sql.ResultSet;

public class Faction {
	private String name, description;
	
	public Faction(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public Faction(ResultSet set) throws Exception {
		this.name = set.getString("name");
		this.description = set.getString("description");
		
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
}


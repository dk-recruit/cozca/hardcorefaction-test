package fr.arthursrv.hcf.faction;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.arthursrv.hcf.HCF;
import fr.arthursrv.hcf.sql.MySQLManager;

public class FactionManager {
	private final HCF core;
	private final ArrayList<Faction> FACTIONS = new ArrayList<>();
	private final ArrayList<FactionPlayer> PLAYERS = new ArrayList<>();
	
	private Statement statement;

	public FactionManager(HCF core) {
		this.core = core;
		try {
			statement = MySQLManager.getConnection().createStatement();
			int factions = 0;
			int players = 0;
			ResultSet set = statement.executeQuery("SELECT * FROM `faction1`");
			while(set.next()) {
				FACTIONS.add(new Faction(set));
				factions++;
			}
			set = statement.executeQuery("SELECT * FROM `faction1`");
			while(set.next()) {
				PLAYERS.add(new FactionPlayer(set, this));
				players++;
			}
			core.getLogger().info("Trouve : " + players + " joueurs et " + factions + " factions");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public FactionPlayer getPlayer(UUID id) {
		for(FactionPlayer fp : PLAYERS) {
			if(fp == null)
				core.getLogger().warning("FactionPlayer in PLAYERS<FactionPlayer> retourne NULL !");
			if(fp.getId().toString() == id.toString() || Bukkit.getOfflinePlayer(id).getName() == Bukkit.getOfflinePlayer(fp.getId()).getName())
				return fp;
		return null;
		}
		return null;
	}
	
	public FactionPlayer getPlayer(Player player) {
		return getPlayer(player.getUniqueId());
	}

	public Faction getFaction(String string) {
		for(Faction f : FACTIONS) {
			if(f.getName().equalsIgnoreCase(string))
				return f;
			
		if(String.valueOf(string) != "null" ) {
			core.getLogger().warning("Le joueur a �t� membre de la faction " + string + " mais la faction est introuvable");
		}
		return null;
		}
		return null;
	}

	public void createPlayerProfile(Player p) {
		try {
			FactionPlayer fp = getPlayer(p);
			if(fp == null) 
				core.getLogger().warning("GetPlayer retourn NULL !");
			statement.executeUpdate("INSERT INTO `players` VALUES ('" + p.getUniqueId() + "', '" + fp.getBalance() + "', '" + fp.getFactionName() + "')");
		} catch (Exception e) {
			core.getLogger().warning("ERREUR LORS DE LA CREATION DU PROFIL DU JOUEUR");
			e.printStackTrace();
			
		}
		
	}
	public void saveProfile(UUID id) {
		try {
			FactionPlayer fp = getPlayer(id);
			core.getLogger().info("Sauvegarde du profil du joueur " + Bukkit.getOfflinePlayer(id).getName());
			statement.executeUpdate("UPDATE `players` WHERE uuid='" + id + "' VALUES ('" + fp.getBalance() + "', '" + fp.getFactionName() + "')");
			core.getLogger().info("Sauvegarde du profil termine");
		} catch (Exception e) {
			core.getLogger().warning("ERREUR LORS DE LA SAUVEGARDE DU PROFIL DU JOUEUR");
			e.printStackTrace();
			
		}
	}

	public void disable() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			saveProfile(p.getUniqueId());
		}
	}

	public void addPlayer(FactionPlayer fp) {
		PLAYERS.add(fp);
	}

}

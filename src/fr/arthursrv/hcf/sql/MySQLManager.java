package fr.arthursrv.hcf.sql;

import java.sql.Connection;
import java.sql.SQLException;

import fr.arthursrv.hcf.Settings;
import pro.husk.mysql.MySQL;
public class MySQLManager {

	private static final MySQL MySQL = new MySQL(Settings.MYSQL_HOST, Settings.MYSQL_PORT, Settings.MYSQL_NAME,Settings.MYSQL_USER, Settings.MYSQL_PSWD, null);
	
	private static Connection connection;
	public static void openConnection() {
		try {
			connection = MySQL.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() {
		return connection;
	}

	public static void closeConnection() {
		try {
			MySQL.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}

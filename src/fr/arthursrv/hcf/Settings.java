package fr.arthursrv.hcf;

public class Settings {

	public static final String  MYSQL_HOST = "localhost";	
	public static final String  MYSQL_PORT = "3306";	
	public static final String  MYSQL_USER = "root";	
	public static final String  MYSQL_PSWD = "password";	
	public static final String  MYSQL_NAME = "faction1";	
	public static final int  MAX_FACTION_CHAR_LENGHT = 20;
	public static final int DEFAULT_BALANCE = 1500;
	
}

package fr.arthursrv.hcf;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import fr.arthursrv.hcf.faction.FactionManager;
import fr.arthursrv.hcf.sql.MySQLManager;

public class HCF extends JavaPlugin {
	
	private static FactionManager MANAGER = null;

	@Override
	public void onEnable() {
		Bukkit.getConsoleSender().sendMessage("HCF demarre");
		MySQLManager.openConnection();
		MANAGER = new FactionManager(this);
		Bukkit.getPluginManager().registerEvents(new HCFListener(), this);
	}
	
	@Override
	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage("HCF s'eteint");
		MANAGER.disable();
		MySQLManager.closeConnection();
	}

	public static FactionManager getManager() {
		
		return MANAGER;
	}
	
	

}

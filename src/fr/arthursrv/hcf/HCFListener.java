package fr.arthursrv.hcf;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.arthursrv.hcf.faction.FactionPlayer;

public class HCFListener implements Listener {
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		FactionPlayer fp = HCF.getManager().getPlayer(e.getPlayer().getUniqueId());
		Player p = e.getPlayer();
		if(fp == null) {
			fp = new FactionPlayer(p, Settings.DEFAULT_BALANCE, null);
			HCF.getManager().addPlayer(fp);
			p.sendMessage("�a�lBienvenue sur le serveur. Recevez comme cadeau de bienvenue 1500 pi�ces, commencez donc par cr�er votre faction ou rejoignez en une !");
			HCF.getManager().createPlayerProfile(p);
		}
		
		e.setJoinMessage(null);
		
	}
		
		@EventHandler
		public void onQuit(PlayerQuitEvent e) {
			Player p = e.getPlayer();
			HCF.getManager().saveProfile(p.getUniqueId());
		
	}
}
